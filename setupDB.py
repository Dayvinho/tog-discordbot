import datetime
from sqlalchemy import (create_engine, Table, Column, UniqueConstraint,
                        Integer, String, MetaData, ForeignKey,
                        DateTime, Binary)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects import mysql
from sqlalchemy.sql import expression, func
from configparser import ConfigParser
import pandas as pd

conf = ConfigParser()
conf.read('config.ini')
ip = conf['DB']['ip']
port = conf['DB']['port']
username = conf['DB']['username']
password = conf['DB']['password']
dbname = conf['DB']['dbname']

engine = create_engine(f'mysql://{username}:{password}@{ip}:{port}/{dbname}')

Base = declarative_base(bind=engine)


class BotUsers(Base):
    __tablename__ = 'bot_users'
    id = Column(Integer, primary_key=True)
    userID = Column(String(50), unique=True, nullable=False)


class Events(Base):
    __tablename__ = 'events'
    id = Column(Integer, primary_key=True)
    msgID = Column(String(50), unique=True, nullable=False)
    channelID = Column(String(50), unique=True, nullable=False)
    name = Column(String(100), nullable=False)
    date = Column(DateTime, nullable=False)
    attendees = Column(mysql.TINYINT(unsigned=True), nullable=False, server_default='9')
    prepTime = Column(mysql.TINYINT(unsigned=True), nullable=False, server_default='15')
    closed = Column(DateTime, nullable=False)
    lastReminder = Column(DateTime, nullable=False, server_default=expression.text('NOW() ON UPDATE NOW()'))
    archived = Column(mysql.BOOLEAN, nullable=False, server_default=expression.false())


class SignedUpUsers(Base):
    __tablename__ = 'signed_up_users'
    id = Column(Integer, primary_key=True)
    userID = Column(String(50), nullable=False)
    msgID = Column(None, ForeignKey('events.msgID'))
    playState = Column(String(25), nullable=False)
    dateCreated = Column(DateTime, nullable=False, server_default=func.now())
    reliable = Column(mysql.BOOLEAN, nullable=False, server_default=expression.true())
    __table_args__ = (UniqueConstraint('userID', 'msgID', name='pKey'),)


class TestEvents(Base):
    __tablename__ = 'test_events'
    id = Column(Integer, primary_key=True)
    msgID = Column(String(50), unique=True, nullable=False)
    channelID = Column(String(50), unique=True, nullable=False)
    name = Column(String(100), nullable=False)
    date = Column(DateTime, nullable=False)
    attendees = Column(mysql.TINYINT(unsigned=True), nullable=False, server_default='9')
    prepTime = Column(mysql.TINYINT(unsigned=True), nullable=False, server_default='15')
    closed = Column(DateTime, nullable=False)
    lastReminder = Column(DateTime, nullable=False, server_default=expression.text('NOW() ON UPDATE NOW()'))
    archived = Column(mysql.BOOLEAN, nullable=False, server_default=expression.false())


class TestSignedUpUsers(Base):
    __tablename__ = 'test_signed_up_users'
    id = Column(Integer, primary_key=True)
    userID = Column(String(50), nullable=False)
    msgID = Column(None, ForeignKey('test_events.msgID'))
    playState = Column(String(25), nullable=False)
    dateCreated = Column(DateTime, nullable=False, server_default=func.now())
    reliable = Column(mysql.BOOLEAN, nullable=False, server_default=expression.true())
    __table_args__ = (UniqueConstraint('userID', 'msgID', name='pKey'),)

print('Dropping Old Tables...')
Base.metadata.drop_all()
print('Creating New Tables...')
Base.metadata.create_all()
print('Tables Created...')

print('Inserting Bot Users...')
df = pd.DataFrame({
    'userID': [
        '262691606543466506',#dayvi
        '309026260124827658',#tommy
        '270250691073802261',#cj
        '401487260337569802']#lukek
    })

df.to_sql('bot_users', con=engine, if_exists='append', index=False)
print('Bot Users Created...')
print('Complete.')