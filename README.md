# README #

### What is this repository for? ###

* Discord bot for maintaining event sign ups.
* Version 0.1


### How do I get set up? ###

* [pip install pipenv](https://pipenv.kennethreitz.org/en/latest/)
* git pull this repository.
* Set up the [DiscordChatExporter.CLI](https://github.com/Tyrrrz/DiscordChatExporter/releases/) as a subfolder of this project (tested with 2.19).
* Run "pipenv install --deploy" in the downloaded repository location, where the Pipfile.lock is located.
* Configuration
	* Requires config.ini with:
		* [DB]
			* ip
			* port
			* username
			* password
			* dbname
		* [BOT]
			* discord_token
			* id
			* archive_delay              Hours after which a closed event channel is moved into the archive category
			* reminder_interval          Hours inbetween sign up reminders, if not enough attendees confirmed attendance
		* [CHANNEL]
			* target_categories          Space separated list of all channel category IDs where commands should work
			* date_format				 https://docs.python.org/3/library/datetime.html#strftime-strptime-behavior
			* archives			         Channel ID where past event exports should end up
			* aggregated_upcoming		 Channel ID where all upcoming events and their current sign up numbers should be posted
* Dependencies
* Database configuration
	* Run setupDB.py for initial DB setup. This project was created using a MySQL database so if planning on using a different type of DB, the 'engine' string in both ToGbot.py & setupDB.py will need to be changed to suit your requirements.
* Deployment instructions

### What are the commands? ###
The delimeter used when entering commands is ' '.
The indented bullet points are the fields that must / can be passed with a command.

Create an event for user to sign up to etc.

* $create_event _(works in all channels of the supplied target_categories)_
	* JSON string
		* 'name'
		* 'date' (YYYY-MM-DD HH:MM) Has to be UTC
		* _'duration'_ [Optional] **Default** 180 Time allotted to the event in minutes
		* _'server_size'_ [Optional] **Default** 36vs36
		* _'attendees'_ [Optional] **Default** 9
		* _'prepTime'_ [Optional] in minutes **Default** 15
		* _'map'_ [Optional] Name should conform to offical layer name on https://squadmaps.com/
		* _'server'_ [Optional]
		* _'teams'_ [Optional] List with 2 entries (1 per side). Each team is its own item in another list.

Example: $create_event {"name":"CCFN", "date":"2020-05-20 19:00", "map":"Al Basrah AAS v1", "teams":[["MP","M"], ["HSR","RvN"]], "attendees":18}

Cancel an event

* $cancel _(works in all channels of the supplied target_categories)_
    * Message ID (msgID) [Optional] Defaults to channelID of used channel if not passed
    
Post final lineup and subtlementary information

* $briefing _(works **only** in created event channels)_
    * _JSON string_ [Optional] If not passed, info is taken from event announcement message
		* _'server_size'_ [Optional]
		* _'attendees'_ [Optional]
		* _'prepTime'_ [Optional] in minutes
		* _'map'_ [Optional] Name should conform to offical layer name on https://squadmaps.com/
		* _'server'_ [Optional]
		* _'teams'_ [Optional] List with 2 entries (1 per side). Each team is its own item in another list.

Example: $briefing {"server": "Coalition Match Server"}

Note somebody not showing up

* $failed_to_appear
	* Message ID (msgID)
	* User ID (userID)

Example: $failed_to_appear 691039457490501633 676420878753464331

Display a list of user ID's that are enabled to use the bot.

* $show_enabled_users

Display a list of events that are yet to take place, it will return the 'msgID', 'eventName' & 'eventDate'

* $show_events

Show the line up for an event.

* $show_lineup
	* Message ID (msgID) [Optional] Tries to use channelID if not passed

Example: $show_lineup 684699312504045568

Close the discord bot.

* $close

### Who do I talk to? ###

* Dave Fenwick - dfenwick@gmail.com
* Marvin Tiedtke

TEST10