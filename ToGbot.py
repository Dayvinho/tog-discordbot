import os
import json
import calendar
import datetime as dt

import discord
from discord.ext import commands, tasks
import pandas as pd
from sqlalchemy import create_engine

from util.constant import *
from util.logger import *
from util.helper import *


bot = commands.Bot(command_prefix='$')

#todo sanitize SQL statements to prevent injection / look into sqlalchemy to replace the current queries?

@bot.event
async def on_ready():
    logger.info(f'Logged in as: {bot.user.name} - {bot.user.id}')
    await bot.change_presence(activity=discord.CustomActivity(name=DEFAULT_STATUS)) # needed to skipp None check during is_debugging
    logger.info('-----------------------------------------')
    for file_name in os.listdir('./cogs'):
        if file_name.endswith('.py'):
            bot.load_extension(f'cogs.{file_name[:-3]}')


@bot.event
async def on_message(message): #maybe replace on cmd / cog basis with @guild_only()
    if isinstance(message.channel, discord.channel.DMChannel) or isinstance(message.channel, discord.channel.GroupChannel):
        return # ignore DMs
    
    await bot.process_commands(message)


@bot.command(aliases=['logout'])
@commands.check(is_authorized)
async def close(ctx):
    await bot.close()


@bot.command()
@commands.check(is_authorized)
async def show_enabled_users(ctx):
    await ctx.send(', '.join(map(str, users)))
    await delete_message(ctx.message)


@bot.command()
@commands.check(is_authorized)
async def add_user(ctx, userID: int):
    global users
    df = pd.DataFrame({
        'userID': userID
    }, index=[0])
    df.to_sql('bot_users', con=engine, if_exists='append', index=False)
    users = reload_users() # update authorized users
    await delete_message(ctx.message)


@bot.command()
@commands.check(is_authorized)
async def archive_category(ctx, categoryID: int, archive_channelID: int):
    category_channel = bot.get_channel(categoryID)
    for channel in category_channel.channels[::-1]:
        await archive_channel(bot, channel, channel.name, archive_channelID)
    await category_channel.delete()
    await delete_message(ctx.message)


bot.run(DISCORD_TOKEN)