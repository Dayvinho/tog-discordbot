import json
import re
import calendar
import datetime as dt
import requests

import pandas as pd

import discord
from discord.ext import commands, tasks
from discord.utils import get #todo

from util.constant import *
from util.logger import *
from util.helper import *
from util.db import *

class Events(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.post_upcoming_event_sign_up_numbers.start()
        logger.info('Started upcoming events sign up numbers background task')
        self.encourage_sign_ups.start()
        logger.info('Started sign up reminder background task')
        self.archive_old_events.start()
        logger.info('Started archive background task')
        self.validate_bot_reactions.start() # todo fuck this thing
        logger.info('Started checking if all reactions are still present on the sign up message')


    def cog_check(self, ctx): # ignore messages from users not registered in bot_users or outside the declared categories
        return is_valid_channel(ctx.channel.category_id) and is_authorized_user(ctx.author.id)
    

    def cog_unload(self):
        self.post_upcoming_event_sign_up_numbers.stop()
        self.encourage_sign_ups.stop()
        self.archive_old_events.stop()
        self.validate_bot_reactions.stop()


    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        channel_category = self.bot.get_channel(payload.channel_id).category_id
        if not is_valid_channel(channel_category):
                return # ignore reactions outside of declared categories

        userID = payload.user_id
        emote = payload.emoji.name
        if userID != self.bot.user.id and emote in REACTIONS:
            events = eventDf['msgID'].tolist()
            msgID = str(payload.message_id)
            if msgID in events:
                logger.info(f'{payload.member.display_name} ({userID}) reacted with {emote} on {msgID}')
                event_message = await get_message_by_id(self.bot, payload.channel_id, msgID)
                reaction = discord.utils.get(event_message.reactions, emoji=emote)

                event = pd.read_sql(f'SELECT name, date FROM events WHERE msgID = {msgID}', con=engine)
                event_name = event.at[0, 'name']
                event_date = event.at[0, 'date']
                if emote == REACTIONS[0]:
                    await signup(payload, ROLES[0], event_name, event_date, event_message.embeds[0].url)
                elif emote == REACTIONS[1]:
                    await signup(payload, ROLES[1], event_name, event_date, event_message.embeds[0].url)
                elif emote == REACTIONS[2]:
                    await signup(payload, ROLES[2], event_name, event_date, event_message.embeds[0].url)
                elif emote == REACTIONS[3]:
                    await signup(payload, ROLES[-1], event_name, event_date, event_message.embeds[0].url)
                else:
                    logger.info(f'Added unknown reaction {emote}')
                await reaction.remove(payload.member)
                await update_event_announcement_message(event_message)
                await update_aggreagted_event_summary(self)
            elif pd.read_sql(f'SELECT EXISTS(SELECT * FROM events WHERE msgID = {msgID})', con=engine).iat[0, 0]:
                logger.info(f'{payload.member.display_name} ({userID}) reacted late with {emote} on {msgID}')
                await self.bot.get_channel(payload.channel_id).send(f'{payload.member.mention} signed up late as {ROLES[REACTIONS.index(emote)]}')


    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, payload):
        channel_category = self.bot.get_channel(payload.channel_id).category_id
        if is_valid_channel(channel_category):
            events = eventDf['msgID'].tolist()
            msgID = str(payload.message_id)
            if msgID in events:
                logger.info(f'{payload.user_id} removed reaction {payload.emoji.name} from {msgID}')
                if payload.user_id == self.bot.user.id:
                    logger.info('People are untrustworthy cunts, thus we need to readd bot reactions')
                    message = await get_message_by_id(self.bot, payload.channel_id, msgID)
                    await message.add_reaction(payload.emoji.name)


    @tasks.loop(hours=1)
    async def archive_old_events(self):
        archivable_events = pd.read_sql(f'SELECT channelID FROM events WHERE ADDTIME(closed, "0 0:0:{ARCHIVE_DELAY}.0") <= UTC_TIMESTAMP() and archived IS false', con=engine)
        conn = engine.connect()
        for _, row in archivable_events.iterrows():
            event = pd.read_sql(f'SELECT * FROM events WHERE channelID = {row["channelID"]}', con=engine)
            file_name = f'{event.at[0, "date"].strftime("%Y-%m-%d %H-%M")}_{event.at[0, "name"]}'.replace(' ', '_')
            await archive_channel(self.bot, self.bot.get_channel(int(row['channelID'])), file_name, ARCHIVES_CHANNEL)
            conn.execute(f'UPDATE events SET archived = true WHERE channelID = {row["channelID"]}')
        conn.close()    


    @tasks.loop(hours=1)
    async def encourage_sign_ups(self):
        upcoming_events = pd.read_sql(f'SELECT channelID, date, missing FROM (SELECT events.msgID, events.channelID, events.date, events.attendees, sign_ups.num, IFNULL(events.attendees - sign_ups.num, events.attendees) AS missing FROM events LEFT JOIN (SELECT msgID, COUNT(*) AS num FROM signed_up_users WHERE playstate = "{ROLES[0]}" GROUP BY msgID) AS sign_ups ON events.msgID = sign_ups.msgID WHERE events.closed > UTC_TIMESTAMP() AND ADDTIME(events.lastReminder, "0 0:0:{REMINDER_INTERVAL}.0") <= UTC_TIMESTAMP()) AS event_attendees WHERE event_attendees.num IS NULL OR event_attendees.num < event_attendees.attendees', con=engine)
        conn = engine.connect()
        for _, row in upcoming_events.iterrows():
            date = row['date'] - dt.timedelta(days=3)
            if date < dt.datetime.now():
                await self.bot.get_channel(int(row['channelID'])).send(f'@everyone We are still {row["missing"]} short!')
            else:
                await self.bot.get_channel(int(row['channelID'])).send(f'We are still {row["missing"]} short!')
            conn.execute(f'UPDATE events SET lastReminder = SUBTIME(UTC_TIMESTAMP(), "0 0:0:1.0") WHERE channelID = {row["channelID"]}')
        conn.close()


    @tasks.loop(hours=REMINDER_INTERVAL, count=NO_SUMMARY_PINGS)
    async def post_upcoming_event_sign_up_numbers(self):
        embed = await create_aggregated_event_summary_embed(self)

        if embed.fields == discord.Embed.Empty:
            return

        channel = self.bot.get_channel(AGGREGATED_UPCOMING_CHANNEL)
        async for message in channel.history(limit=1):
            await message.delete()
        
        if NO_SUMMARY_PINGS != 1:
            has_missing = False
            for i, field in enumerate(embed.fields):
                if i % 3 == 1 and int(field.value) > 0:
                    has_missing = True
                    break

            if has_missing:
                await channel.send('@everyone', embed=embed)
                return

        await channel.send(embed=embed)


    @tasks.loop(minutes=15) # todo find cause instead of bandaid
    async def validate_bot_reactions(self): # no idea why this crap is needed, but for some reason bot reactions can disappear
        for _, row in eventDf.iterrows(): # as in reaction.clear is called instead of remove
            msg = await get_message_by_id(self.bot, int(row['channelID']), int(row['msgID']))
            if len(msg.reactions) != 4:
                logger.warning(f'Message {row["msgID"]} was missing a reaction for GOD KNOWNS WHICH FUCKING REASON')
                await msg.clear_reactions()
                await add_sign_up_reactions(msg)


    @commands.command(brief='Lists all upcoming events')
    async def show_events(self, ctx):
        for _, row in eventDf.iterrows():
            await ctx.send(f'{row["msgID"]} - {row["name"]} - {row["date"]}')
        await delete_message(ctx.message)


    @commands.command(brief='Lists all sign ups to an event')
    async def show_lineup(self, ctx, msgID: int = None):
        ids = get_IDs(msgID, ctx)
        if not ids:
            return

        event_lineup = get_event_lineup(ids['msg'])
        for _, row in event_lineup.iterrows():
            await ctx.send(f'<@{int(row["userID"])}> - {row["playState"]}')
        await delete_message(ctx.message)


    @commands.command(usage='')
    async def create_event(self, ctx, *, config):
        global eventDf
        config = json.loads(config)
        date = dt.datetime.strptime(config['date'], '%Y-%m-%d %H:%M')
        category_channel = self.bot.get_channel(ctx.channel.category_id)
        channel = await category_channel.create_text_channel(f'{date.strftime(CHANNEL_DATE_FORMAT)}_{config["name"]}'.replace(' ', '_'))

        event_channels = []
        first_event_index = 0
        for chan in category_channel.text_channels:
            if chan.name[0].isdigit():
                event_channels.append(chan)
                if not first_event_index:
                    first_event_index = chan.position
        sorted_channels = sorted(event_channels, key=lambda  x: dt.datetime.strptime(x.name[:CHANNEL_DATE_LENGTH], CHANNEL_DATE_FORMAT))
        correct_channel_position = sorted_channels.index(channel)
        if correct_channel_position != len(event_channels) - 1:
            await channel.edit(position=correct_channel_position + first_event_index)
        
        if 'server_size' not in config:
            config['server_size'] = '36vs36'
        if 'attendees' not in config:
            config['attendees'] = '9'
        if 'prepTime' not in config:
            config['prepTime'] = 15

        msg = await channel.send('@everyone', embed=create_event_embed(config, date))

        df = pd.DataFrame({
            'msgID': msg.id,
            'channelID': channel.id,
            'name': config['name'],
            'date': date,
            'closed': date,
            'attendees': config['attendees'],
            'prepTime': config['prepTime']
        }, index=[0])
        df.to_sql('events', con=engine, if_exists='append', index=False)

        eventDf = reload_events() # update active events
        await msg.pin()
        await add_sign_up_reactions(msg)
        await update_aggreagted_event_summary(self)
        await delete_message(ctx.message)


    @commands.command()
    async def cancel(self, ctx, msgID: int = None):
        ids = get_IDs(msgID, ctx)
        if not ids:
            return

        global eventDf
        conn = engine.connect()
        conn.execute(f'UPDATE events SET closed = UTC_TIMESTAMP() WHERE msgID = {ids["msg"]}')
        conn.close()
        eventDf = reload_events() # update active events

        event_message = await get_message_by_id(self.bot, ids['channel'], ids['msg'])
        event_embed = event_message.embeds[0]
        cancel_msg = f'{event_embed.title} at {event_embed.fields[0].value[5:21]} is unfortunately canceled.'
        await ctx.send(cancel_msg)
        event_lineup = get_event_lineup(ids['msg'])
        for _, row in event_lineup.iterrows():
            await self.bot.get_user(int(row['userID'])).send(f'{cancel_msg} You are no longer considered as {row["playState"]}.')
        await update_aggreagted_event_summary(self)
        await delete_message(ctx.message)


    @commands.command()
    async def briefing(self, ctx, *, config = None):
        global eventDf
        if config:
            config = json.loads(config)

        msgID = get_IDs(None, ctx)['msg']

        conn = engine.connect()
        if config:
            if 'attendees' in config:
                conn.execute(f'UPDATE events SET attendees = {config["attendees"]} WHERE msgID = {msgID}')
            if 'prepTime' in config:
                conn.execute(f'UPDATE events SET prepTime = {config["prepTime"]} WHERE msgID = {msgID}')
        conn.execute(f'UPDATE events SET closed = UTC_TIMESTAMP() WHERE msgID = {msgID}')
        conn.close()
        eventDf = reload_events() # update active events

        event_message = await get_message_by_id(self.bot, ctx.channel.id, msgID)
        embed = create_briefing_embed(event_message.embeds[0], get_event_lineup(msgID), config=config)
        event = pd.read_sql(f'SELECT * FROM events WHERE msgID = {msgID}', con=engine)
        time_to_event = event.at[0, 'date'] - dt.datetime.utcnow()
        total_seconds = int(time_to_event.total_seconds())
        hours, remainder = divmod(total_seconds, 60 * 60)
        minutes = divmod(remainder, 60)[0]
        time_to_event = '{} hrs {} mins'.format(hours, minutes)
        for field in embed.fields[-4:]:
            for row in field.value.splitlines():
                match = re.search('<@(.+?)>', row)
                if match:
                    await self.bot.get_user(int(match.group(1))).send(f'You will be considered as {field.name} for {event.at[0, "name"]} going ahead as scheduled in {time_to_event}. Please show up at least {event.at[0, "prepTime"]} minutes earlier on TS.')

        await ctx.send(f'If you are unaccounted for {event.at[0,"prepTime"]} minutes before game start, you may be replaced', embed=embed)
        await update_aggreagted_event_summary(self)
        await delete_message(ctx.message)


    @commands.command()
    async def failed_to_appear(self, ctx, msgID: int, userID: int):
        conn = engine.connect()
        conn.execute(f'UPDATE signed_up_users SET reliable = false WHERE msgID = {msgID} AND userID = {userID}')
        conn.close()
        await delete_message(ctx.message)


def get_event_lineup(msgID):
    df = pd.read_sql(f'SELECT * FROM signed_up_users WHERE msgID = {msgID} AND NOT playState = "{ROLES[-1]}" ORDER BY playState, dateCreated', con=engine)
    logger.info(df)
    return df


def get_IDs(msgID, ctx):
    if msgID is None:
        msg = pd.read_sql(f'SELECT msgID FROM events WHERE channelID = {ctx.channel.id}', con=engine)
        if msg.size > 0:
            return {'msg': msg.at[0, 'msgID'], 'channel': ctx.channel.id}
        else:
            logger.info(f'Command call in invalid channel: "{ctx.message.content}" in channel: {ctx.channel.id}') # todo DM help
            return
    else:
        chn = pd.read_sql(f'SELECT channelID FROM events WHERE msgID = {msgID}', con=engine)
        return {'msg': msgID, 'channel': chn.at[0, 'channelID']}


async def signup(payload, play_state, event_name, event_date, calendar):
    db_entry(payload.user_id, payload.message_id, play_state)
    embed = discord.Embed(title='Click to add to Google calendar', url=f'{calendar}&details={play_state.replace(" ", "+")}')
    if play_state == ROLES[-1]:
        embed = None
    await payload.member.send(f'You registered as **{play_state}** for **{event_name}** at **{event_date}**.', embed=embed)


def create_event_embed(config, date):
    timeIS = f'https://time.is/{date.hour}{date.minute:02d}_{date.day}_{calendar.month_abbr[date.month]}_' \
                f'{date.year}_in_UTC?{config["name"].replace(" ", "_")}'

    if 'duration' not in config:
        config['duration'] = 180

    embed = discord.Embed(
        title=config['name'],
        description=f'Sign up via {REACTIONS[0]} reaction.\n' +
                    f'If you want to play, but are unsure if you\'ll make it in time, react with {REACTIONS[1]}.\n' +
                    f'To sign up as **reserve**, i.e. play if we are short, react with {REACTIONS[2]}. \n' +
                    f'If you are unable to play, please sign off via {REACTIONS[3]}.',
        colour=discord.Colour.blue(),
        url=f'https://calendar.google.com/calendar/r/eventedit?text={config["name"].replace(" ", "+")}&dates={to_google_calendar_link_date(date - dt.timedelta(minutes=int(config["prepTime"])))}/{to_google_calendar_link_date(date + dt.timedelta(minutes=config["duration"]))}')
    embed.set_footer(text='Sign up for shit, cockwombles')
    embed.add_field(name=DATE, value=f'[{calendar.day_abbr[date.weekday()]} {date.strftime("%d-%m-%Y %H:%M")}]({timeIS})', inline=True)
    embed.add_field(name=SERVER_SIZE, value=config['server_size'], inline=True)
    embed.add_field(name=ATTENDEES, value=config['attendees'], inline=True)
        
    embed = add_optional_embed_fields(embed, config)

    embed.add_field(name=ROLES[0], value=EMPTY_LINE, inline=False)
    embed.add_field(name=ROLES[1], value=EMPTY_LINE, inline=False)
    embed.add_field(name=ROLES[2], value=EMPTY_LINE, inline=False)

    embed.timestamp = dt.datetime.utcnow()
    return embed


def to_google_calendar_link_date(date):
    return date.isoformat().replace(":", "").replace("-", "") + 'Z'

def create_briefing_embed(old_embed, event_lineup, config=None):
    attendees = 0
    embed = discord.Embed(title=old_embed.title,
        colour=discord.Colour.blue())
    embed.set_footer(text="You're all a bunch of cunts")
    for field in old_embed.fields:
        if field.name == ATTENDEES:
            attendees = field.value
        if field.name not in ROLES:
            embed.add_field(name=field.name, value=field.value, inline=field.inline)

    if config:
        embed = add_optional_embed_fields(embed, config)
        if 'attendees' in config: # to accept more then the predefined number of attendees
            attendees = config['attendees']
            embed.set_field_at(2, name=ATTENDEES, value=attendees)

    attendees = int(attendees)
    playing = priority_reserve = qualified_reserve = contingency_reserve = ('', 1)
    for _, entry in event_lineup.iterrows():
        if entry['playState'] == ROLES[0] and playing[1] <= attendees and not int(entry['userID']) == 225351432759541760:
            playing = expand_lineup(playing, entry['userID'])
        elif entry['playState'] == ROLES[0]:
            priority_reserve = expand_lineup(priority_reserve, entry['userID'])
        elif entry['playState'] == ROLES[1]:
            qualified_reserve = expand_lineup(qualified_reserve, entry['userID'])
        elif entry['playState'] == ROLES[2]:
            contingency_reserve = expand_lineup(contingency_reserve, entry['userID'])

    embed.add_field(name=ROLES[0], value=get_formated_lineup(playing), inline=False)
    embed.add_field(name='Priority reserve', value=get_formated_lineup(priority_reserve), inline=False)
    embed.add_field(name='Reserve if present', value=get_formated_lineup(qualified_reserve), inline=False)
    embed.add_field(name='Reserve if needed', value=get_formated_lineup(contingency_reserve), inline=False)

    embed.timestamp = dt.datetime.utcnow()
    return embed


def expand_lineup(lineup_tuple, participant: int):
    return (f'{lineup_tuple[0]}{lineup_tuple[1]}.) <@{participant}>\n', lineup_tuple[1] + 1)


def get_formated_lineup(lineup_tuple):
    return lineup_tuple[0] if lineup_tuple[0] else EMPTY_LINE


def add_optional_embed_fields(embed, config):
    map_index = team1_index = server_index = 0
    for i, item in enumerate(embed.fields):
        if item.name == MAP:
            map_index = i
        elif item.name == TEAMS[0]:
            team1_index = i
        elif item.name == SERVER:
            server_index = i

    if 'map' in config:
        map_url = map_value = None
        try:
            map_url = f'https://squadmaps.com/img/maps/full_size/{config["map"].replace(" ", "_")}.jpg'
            request = requests.get(map_url)
            if request.status_code < 400:
                map_value = f'[{config["map"]}]({map_url})'
            else: # markdown link; format [text](http_link)
                map_url = config['map'].split('(',1)[1][:-1]
                request = requests.get(map_url)
                map_value = config['map']
        except:
            map_url = discord.Empty
            map_value = config['map']
        
        if map_index:
            embed.set_field_at(index=map_index, name=MAP, value=map_value)
        else:
            embed.add_field(name=MAP, value=map_value)
            embed.set_image(url=map_url)
    if 'teams' in config:
        team1 = '\n'.join(config['teams'][0])
        team2 = '\n'.join(config['teams'][1])
        if team1_index:
            embed.set_field_at(index=team1_index, name=TEAMS[0], value=team1)
            embed.set_field_at(index=team1_index + 1, name=TEAMS[1], value=team2)
        else:    
            embed.add_field(name=TEAMS[0], value=team1)
            embed.add_field(name=TEAMS[1], value=team2)
    if 'server' in config:
        if server_index:
            embed.set_field_at(index=server_index, name=SERVER, value=config['server'])
        else:
            embed.add_field(name=SERVER, value=config['server'])
    return embed


async def update_event_announcement_message(message):
    embed = message.embeds[0]
    for i, item in enumerate(embed.fields):
        if item.name in ROLES:
            db_query = pd.read_sql(f'SELECT * FROM signed_up_users WHERE msgID = {message.id} AND playState = "{item.name}" ORDER BY dateCreated', con=engine)
            embed.set_field_at(index=i, name=item.name, inline=item.inline, value=create_lineup_string(db_query))
    await message.edit(embed=embed)
    

async def create_aggregated_event_summary_embed(self):
    upcoming_events = pd.read_sql(f'SELECT channelID, msgID, case when missing < 0 then 0 else missing end missing, case when reserve < 0 then 0 else reserve end reserve from (SELECT channelID, msgID, cAttendees, IFNULL(cAttendees - playing, cAttendees) as missing, IFNULL(available - cAttendees, 0) as reserve FROM (SELECT events.msgID, events.channelID, events.date, cast(events.attendees as SIGNED) as cAttendees, sign_ups.playing, sign_ups.available FROM events LEFT JOIN (SELECT msgID, cast(count(CASE when playstate = "{ROLES[0]}" then 1 end) as signed) as playing, cast(count(CASE WHEN playstate = "{ROLES[0]}" or playstate = "{ROLES[2]}" then 1 end) as signed) as available from signed_up_users GROUP BY msgID) AS sign_ups ON events.msgID = sign_ups.msgID WHERE events.closed > UTC_TIMESTAMP() order by events.date ASC) AS event_attendees) as upcoming_events', con=engine)

    embed = discord.Embed(
        title='Upcoming events',
        description='**Missing** = *NEEDED* attendees - *PLAYING* attendees.\n**Reserves** = *PLAYING* attendees + *RESERVE* attendees - *NEEDED* attendees',
        colour=discord.Colour.blue())
    
    for _, row in upcoming_events.iterrows():
        msg = await get_message_by_id(self.bot, int(row['channelID']), int(row['msgID']))
        embed.add_field(name='Event', value=f'[{self.bot.get_channel(int(row["channelID"])).name}]({msg.jump_url})', inline=True)
        embed.add_field(name='Missing', value=row['missing'], inline=True)
        embed.add_field(name='Reserves', value=row['reserve'], inline=True)
    
    return embed


async def update_aggreagted_event_summary(self):
    channel = self.bot.get_channel(AGGREGATED_UPCOMING_CHANNEL)
    embed = await create_aggregated_event_summary_embed(self)
    async for message in channel.history(limit=1):
        await message.edit(embed=embed)


async def add_sign_up_reactions(message):
    await message.add_reaction(REACTIONS[0])
    await message.add_reaction(REACTIONS[1])
    await message.add_reaction(REACTIONS[2])
    await message.add_reaction(REACTIONS[3])


def create_lineup_string(db_query):
    lineup = ''
    if len(db_query.index) > 0:
        for row, entry in db_query.iterrows():
            lineup = f'{lineup}{row + 1}.) <@{int(entry["userID"])}>\n'
    else:
        lineup = EMPTY_LINE
    return lineup


def is_valid_channel(category_id):
    return category_id in TARGET_CATEGORIES


def is_authorized_user(id):
    return id in users


def setup(bot):
    bot.add_cog(Events(bot))