import logging

import discord
from discord.ext import commands
from discord.ext.commands import MemberConverter

from util.logger import *
from util.helper import *

class Debug(commands.Cog):
    def __init__(self, bot):
        self.bot = bot


    def cog_check(self, ctx):
        return is_authorized(ctx)
    
    @commands.command()
    async def debug(self, ctx):
        await self.bot.change_presence(activity=discord.CustomActivity(name=DEBUG_STATUS))
        fh.setLevel(logging.DEBUG)
        await delete_message(ctx.message)


    @commands.command()
    async def end_debug(self, ctx):
        await self.bot.change_presence(activity=discord.CustomActivity(name=DEFAULT_STATUS))
        fh.setLevel(logging.ERROR)
        await delete_message(ctx.message)

    
    @commands.command()
    async def reload_cogs(self, ctx):
        for file_name in os.listdir('./cogs'):
            if file_name.endswith('.py'):
                self.bot.unload_extension(f'cogs.{file_name[:-3]}')
                self.bot.load_extension(f'cogs.{file_name[:-3]}')
        await delete_message(ctx.message)


    @commands.command()
    async def test(self, ctx):
        converter = MemberConverter()
        member = await converter.convert(ctx, self.bot.id)
        logger.info(member.display_name)


def setup(bot):
    bot.add_cog(Debug(bot))