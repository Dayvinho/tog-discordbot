import datetime as dt
import asyncio

import discord
from discord.ext import commands, tasks
from discord.utils import get, sleep_until

from util.constant import *
from util.logger import *
from util.helper import *

class Members(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        guild = bot.get_guild(GUILD_ID)
        self.RECRUIT_ROLE = get(guild.roles, id=RECRUIT_ID)
        self.MEMBER_ROLE = get(guild.roles, id=MEMBER_ID)
        

    def cog_check(self, ctx):
        return is_authorized(ctx) and LOBBY_ID == ctx.channel.id


    @commands.Cog.listener()
    async def on_member_join(self, member): # todo default role
        pass
    

    @commands.Cog.listener()
    async def on_member_remove(self, member):
        await self.bot.get_channel(LOBBY_ID).send(f'{member.mention} left the server')
    

    @commands.command(brief='Assigns a member to the recruit role and sends a reminder after the trail period is up')
    async def recruit(self, ctx, *, member: discord.Member):
        await member.edit(nick=f'{RECRUIT_TAG} {member.display_name}')
        await member.add_roles(self.RECRUIT_ROLE)
        await asyncio.sleep(TRAIL_PERIOD * 24 * 60 * 60) #todo background task to make it restart prove
        msg = await ctx.send(f'Default trail period of {member.display_name} is up. Promote to full member?')
        await msg.add_reaction(REACTION_APPROVE)

        def check(reaction):
            return reaction.user_id != self.bot.user.id and str(reaction.emoji) == REACTION_APPROVE

        await self.bot.wait_for('raw_reaction_add', check=check)
        await member.edit(nick=f'{MEMBER_TAG} {member.display_name[len(RECRUIT_TAG):]}')
        await member.remove_roles(self.RECRUIT_ROLE)
        await member.add_roles(self.MEMBER_ROLE)

def setup(bot):
    bot.add_cog(Members(bot))