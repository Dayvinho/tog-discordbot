import logging, logging.handlers

logger = logging.getLogger('')
logger.setLevel(logging.DEBUG)
log_format = '%(asctime)s - %(levelname)s - %(message)s'
formatter = logging.Formatter(log_format)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
ch.setFormatter(formatter)
logger.addHandler(ch)
fh = logging.FileHandler(filename='bot.log', encoding='utf-8')
fh.setLevel(logging.WARNING)
fh.setFormatter(formatter)
logger.addHandler(fh)
fh_debug = logging.handlers.RotatingFileHandler(filename='debug.log', encoding='utf-8', maxBytes=2000000, backupCount=10)
fh_debug.setLevel(logging.INFO)
fh_debug.setFormatter(formatter)
logger.addHandler(fh_debug)