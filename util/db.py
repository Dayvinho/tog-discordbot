import pandas as pd
from sqlalchemy import create_engine

from util.constant import *

engine = create_engine(f'mysql://{USERNAME}:{PASSWORD}@{IP}:{PORT}/{DBNAME}')


def reload_events():
    global eventDf
    eventDf = pd.read_sql(f'SELECT msgID, channelID, name, date FROM events WHERE closed > UTC_TIMESTAMP()', con=engine) # global eventDf
    return eventDf


def reload_users():
    global users
    users = pd.read_sql('SELECT userID FROM bot_users', con=engine)
    users = [int(item) for sublist in users.values.tolist() for item in sublist] # global users
    return users


def db_entry(userID, msgID, playState):
    conn = engine.connect()
    conn.execute(f'REPLACE INTO signed_up_users (userID, msgID, playState, dateCreated) VALUES ({userID}, {msgID}, "{playState}", UTC_TIMESTAMP())')
    conn.close()


eventDf = reload_events()
users = reload_users()