import os
import subprocess

import discord

from util.constant import *
from util.logger import *


async def is_authorized(ctx):
    return any(role.id == ADMIN_ROLE_ID for role in ctx.author.roles)


async def delete_message(message):
    logger.info(f'Deleted message by {message.author} : {message.content}')
    await message.delete()


async def get_message_by_id(bot, channelID, msgID):
    channel = bot.get_channel(channelID)
    return await channel.fetch_message(msgID)


async def archive_channel(bot, channel, file_name, archiveID: int):
    path = CHAT_EXPORTER_PATH
    if not path:
        return

    file_path = f'.{os.sep}archive{os.sep}{file_name}.html'
    subprocess.Popen([path, 'export', '-t', DISCORD_TOKEN, '-b', '-c', str(channel.id), '-o', file_path, '--dateformat', 'u'], shell=True).wait()
    await bot.get_channel(archiveID).send(file=discord.File(file_path))
    await channel.delete()


def is_debugging(bot):
    return bot.get_guild(GUILD_ID).get_member(bot.user.id).activity.name == DEBUG_STATUS