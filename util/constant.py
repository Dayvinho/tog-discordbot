import os
from configparser import ConfigParser
from datetime import datetime

conf = ConfigParser()
conf.read('config.ini')
DISCORD_TOKEN = conf['BOT']['discord_token']
GUILD_ID = int(conf['BOT']['guild'])
ADMIN_ROLE_ID = int(conf['BOT']['admin'])
IP = conf['DB']['ip']
PORT = conf['DB']['port']
USERNAME = conf['DB']['username']
PASSWORD = conf['DB']['password']
DBNAME = conf['DB']['dbname']
ARCHIVE_DELAY = int(conf['EVENTS']['archive_delay'])
REMINDER_INTERVAL = int(conf['EVENTS']['reminder_interval'])
TARGET_CATEGORIES = conf['EVENTS']['target_categories']
TARGET_CATEGORIES = [int(x) for x in TARGET_CATEGORIES.split(' ')]
CHANNEL_DATE_FORMAT = conf['EVENTS']['date_format']
ARCHIVES_CHANNEL = int(conf['EVENTS']['archives'])
AGGREGATED_UPCOMING_CHANNEL = int(conf['EVENTS']['aggregated_upcoming'])
NO_SUMMARY_PINGS = int(conf['EVENTS']['no_summary_pings'])
LOBBY_ID = int(conf['MEMBERS']['lobby'])
RECRUIT_ID = int(conf['MEMBERS']['recruit_id'])
RECRUIT_TAG = conf['MEMBERS']['recruit_tag']
MEMBER_ID = int(conf['MEMBERS']['member_id'])
MEMBER_TAG = conf['MEMBERS']['member_tag']
TRAIL_PERIOD = int(conf['MEMBERS']['trail_period'])

CHANNEL_DATE_LENGTH = len(datetime.strftime(datetime.utcnow(), CHANNEL_DATE_FORMAT))

DEFAULT_STATUS = ''
DEBUG_STATUS = 'Debugging'


ROLES = ('Playing', 'Playing if present', 'Reserve', 'Not Playing')
EMPTY_LINE = '\r\u200b'

# embed fields
DATE = 'UTC Date'
SERVER_SIZE = 'Server size'
ATTENDEES = 'Needed attendees'
MAP = 'Map'
TEAMS = ('Team 1', 'Team 2')
SERVER = 'Server'

REACTIONS = ('⬆️', '🕛', '🃏', '⬇️')
REACTION_APPROVE = '✅'

def get_chat_exporter_path():
    if os.name == 'nt': # windows environment
        return f'.{os.sep}DiscordChatExporter.CLI{os.sep}DiscordChatExporter.Cli.exe'
    elif os.name == 'posix': # linux environment
        return f'dotnet .{os.sep}DiscordChatExporter.CLI{os.sep}DiscordChatExporter.Cli.dll'
    else:
        return

CHAT_EXPORTER_PATH = get_chat_exporter_path()